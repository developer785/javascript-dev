const _w        = window,
    _d          = _w.document,
    _l          = _w.location,
    _php        = _dir + 'php/',
    _valNombre  = /^[a-zA-Z\-\'\#]+$/,
    _valCorreo  = /[\w-\.]{3,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/,
    _valRut     = /^[1-9][0-9]{6,7}-[0-9kK]/,
    _valFecha   = /^(?:(?:(0?[1-9]|1\d|2[0-8])[/](0?[1-9]|1[0-2])|(29|30)[/](0?[13-9]|1[0-2])|(31)[/](0?[13578]|1[02]))[/](0{2,3}[1-9]|0{1,2}[1-9]\d|0?[1-9]\d{2}|[1-9]\d{3})|(29)[/](0?2)[/](\d{1,2}(?:0[48]|[2468][048]|[13579][26])|(?:0?[48]|[13579][26]|[2468][048])00))$/,

    _cargaAx1       = '<div id="animacion-ajax" class="animacion-carga"></div>',
    _cargaAx2       = '<div id="animacion-ajax" class="animacion-carga chica"></div>',
    _cargaAx3       = '<div id="animacion-ajax-2" class="animacion-carga"></div>',
    _cargaAx4       = '<div id="animacion-ajax-2" class="animacion-carga chica"></div>',
    _htmlSeparacion = '<br><br><div class="linea-separadora"></div><br><br>';

var JSON        = JSON || {},
    _debug      = _debug || false,
    _wFocus     = true,
    _timer      = null,
    _ev         = null,
    _archivosJS = _archivosJS || [],
    _urlAx;

function _nuevoEl(tipo, datos, como_html){
    var el          = _d.createElement(tipo),
        datos       = datos || false,
        como_html   = como_html || false,
        p;

    if(datos){
        for(p in datos)
            el[p] = datos[p];
    }

    return (!como_html)? el : el.outerHTML;
}

function _enlaceFalso(evento){
    var evento = evento || _w.event;
    evento.preventDefault();
}

function _anchoDispositivo(){
    var ancho = _w.innerWidth || _w.outerWidth;
    return ancho;
}

function _altoDispositivo(){
    var alto = _w.innerHeight || _w.outerHeight;
    return alto;
}

function _esMovil(px){
    var px = parseInt(px) || 991;
    return (_anchoDispositivo() <= px);
}

function _recargar(){
    _l.href = '';
}

function _redir(pagina, segundos){
    var e           = event,
        pagina      = pagina || '',
        segundos    = segundos || 0;

    if(pagina != '')
        pagina += '/';

    pagina = _dir + pagina;

    setTimeout(() => {
        if(!e.ctrlKey)
            _l.href = pagina;
        else
            _w.open(pagina, '_blank');
    }, segundos);
}

function _hashUrl(){
    return _l.hash.replace('#', '');
}

function _urlReferencia(){
    return _d.referrer;
}

function _urlActual(){
    return _l.pathname;
}

function _paginaAnterior(){
    _w.history.back();
}

function _moverScroll(alto, movimiento, el){
    var movimiento  = movimiento || false,
        el          = el || _w;

    el.scroll({
        top: alto,
        left: 0,
        behavior: (!movimiento)? 'auto' : 'smooth'
    });
}

function _anadirEvento(elementos, tipo_evento, funcion){
    var elementos   = elementos || [],
        total       = elementos.length || 0,
        funcion     = funcion || false;

    if(funcion !== false){
        if(!total){
            elementos   = [elementos];
            total       = elementos.length;
        }

        if(total > 0){
            elementos.forEach(e => {
                if(_esObjeto(e)){
                    if(e.addEventListener)
                        e.addEventListener(tipo_evento, function(event){ _ev = event; funcion(event); }, false); // Navegadores Regulares
                    else if(e.attachEvent)
                        e.attachEvent('on' + tipo_evento, function(event){ _ev = event; funcion(event); }, false); // Internet Explorer
                    else
                        e['on' + tipo_evento] = function(event){ _ev = event; funcion(event); }; // Otros
                }
            });
        }
    }
}

function _quitarEvento(elementos, tipo_evento, funcion){
    var elementos   = elementos || [],
        total       = elementos.length || false,
        funcion     = funcion || false,
        i, actual;

    if(funcion !== false){
        if(!total){
            elementos   = [elementos];
            total       = elementos.length;
        }

        if(total > 0){
            elementos.forEach(e => {
                if(_esObjeto(e)){
                    if(e.removeEventListener)
                        e.removeEventListener(tipo_evento, funcion, false); // Navegadores Regulares
                    else if(e.removeEvent)
                        e.removeEvent('on' + tipo_evento, funcion, false); // Internet Explorer
                    else
                        e['on' + tipo_evento] = null; // Otros
                }
            });
        }
    }
}

function _ejecutar(){
    var funcion = arguments[0] || false;

    if(!funcion)
        return false;

    if(_esFuncion(funcion))
        ejecuta_f = funcion;
    else if(_existeFuncion(funcion))
        ejecuta_f = _w[funcion];

    if(ejecuta_f !== null)
        ejecuta_f.apply(null, Array.prototype.slice.call(arguments, 1));
    else
        console.log('No existe funcion', funcion);
}

function _ejecutarListado(funciones){
    var funciones   = funciones || false,
        total       = funciones.length || 0;

    if(!funciones || !_esObjeto(funciones) || total < 1)
        return false;

    funciones.forEach(f => _ejecutar.apply(null, f));
}

function _existeFuncion(nombre, objeto){
    var nombre = nombre || false,
        objeto = objeto || _w;

    return (nombre !== false && _esFuncion(objeto[nombre]));
}

function _esUndefined(valor){
    return (typeof valor === 'undefined');
}

function _esNumero(valor){
    return (typeof valor === 'number');
}

function _esString(valor){
    return (typeof valor === 'string');
}

function _esFuncion(valor){
    return (typeof valor === 'function');
}

function _esObjeto(valor){
    return (typeof valor === 'object');
}

function _esArreglo(valor){
    return Array.isArray(valor);
}

function _arregloAObjeto(arr){
    var arr = arr || [],
        aux = {};

    if(_esArreglo(arr)){
        arr.forEach((v,k) => aux[k] = v);
        arr = aux;
    }

    return arr;
}

function _llavesObjeto(obj, arreglo){
    if(!_esObjeto(obj))
        return 0;

    var arreglo = arreglo || false,
        llaves  = (_esUndefined(Object.keys(obj)) || Object.keys(obj) == null)? 'NULL' : Object.keys(obj);

    return (arreglo === false)? llaves.length : llaves;
}

function _concatenarObj(objs){
    var objs    = objs || [],
        obj     = {},
        total   = objs.length || 0,
        i;

    if(total > 0){
        objs.forEach(o => {
            if(_esObjeto(o)){
                for(i in o)
                    obj[i] = o[i];
            }
        });
    }

    return obj;
}

function _htmlAttr(datos, evitar){
    var datos   = datos || {},
        evitar  = evitar || [],
        html    = '',
        i;

    for(i in datos){
        if(!evitar.existeValor(i))
            html += ' ' + i + '="' + datos[i] + '"';
    }

    return html;
}

function soloNumeros(e){
    var tecla = (_w.event)? _w.event.keyCode : e.which;

    if((tecla == 0) || (tecla == 8) || (tecla == 37) || (tecla == 39) || (tecla == 46))
        return true;

    return /\d/.test(String.fromCharCode(tecla));
}

function dejarSoloNumero(input, es_rut){
    var input   = _(input),
        texto   = input.enMinuscula(),
        total   = input.largoTexto(),
        ultimo  = total - 1,
        es_rut  = es_rut || false,
        valor   = '',
        i, actual, es_ultimo;

    if(total > 0){
        for(i = 0; i < total; ++i){
            actual      = texto[i];
            es_ultimo   = (i == ultimo);

            if(!isNaN(actual) || (es_rut === true && actual === 'k' && es_ultimo)){
                if(es_rut === true && i > 0 && es_ultimo)
                    valor += '-';

                valor += input.caracter(i);
            }
        }
    }

    input.valor(valor);
    return valor;
}

function dejarFormatoNombre(input, con_numero, arr_aux){
    var input       = _(input),
        con_numero  = con_numero || false,
        total       = input.largoTexto(),
        minuscula   = input.enMayuscula(),
        mayuscula   = input.enMinuscula(),
        arr_esp     = ["'", '-'],
        arr_aux     = arr_aux || [],
        valor       = '',
        i, actual;

    for(i = 0; i < total; ++i){
        actual = minuscula[i];

        if(
            actual != mayuscula[i] ||
            arr_esp.existeValor(actual) ||
            arr_aux.existeValor(actual) ||
            (con_numero === true && !isNaN(actual))
        ){
            valor += input.caracter(i);
        }
    }

    input.valor(valor);
    return valor;
}

function segundos(valor){
    return valor * 1000;
}

function distanciaEfecto(posicion_elemento, porcentaje){
    var porcentaje  = porcentaje || 1.1,
        altura      = _altoDispositivo();

    return posicion_elemento - parseInt(altura / porcentaje);
}

function puntos(valor){
    var texto   = valor.toString(),
        decimal = texto.split(','),
        numero  = decimal[0],
        total   = numero.length,
        aux     = '',
        aux_2   = '',
        caracter, i;

    for(i = total; i >= 0; --i){
        caracter = numero.charAt(i);

        if(!isNaN(caracter))
            aux += caracter;
    }

    total = aux.length;
    for(i = total; i >= 0; --i){
        aux_2 += aux.charAt(i);

        if(i < total && i > 0 && i % 3 == 0)
            aux_2 += '.';
    }

    if(!_esUndefined(decimal[1]))
        aux_2 += ',' + decimal[1];

    return aux_2;
}

String.prototype.trim = function(){
    return this.replace(/\s*$/g, '').replace(/^\s*/g, '');
}

String.prototype.extraerScript = function(){
    return this.replace(/<script.*\/script>/isg, '').replace(/<script.*?$/isg, '').trim();
};

//String.prototype.puntos = function(){ };

String.prototype.devuelveDigito = function(){
    var rut  = this,
        l    = rut.length,
        suma = rut.charAt(l - 9) * 4
            + rut.charAt(l - 8) * 3
            + rut.charAt(l - 7) * 2
            + rut.charAt(l - 6) * 7
            + rut.charAt(l - 5) * 6
            + rut.charAt(l - 4) * 5
            + rut.charAt(l - 3) * 4
            + rut.charAt(l - 2) * 3
            + rut.charAt(l - 1) * 2;

    var resto       = suma % 11,
        verificador = 11 - resto;

    if(resto == 0)
        verificador = 0;
    else if(verificador == 10)
        verificador = 'k';

    return verificador;
}


JSON.stringify = JSON.stringify || function(obj){
    var t = typeof(obj);

    if(t != "object" || obj === null){
        // simple data type
        if (t == "string")
            obj = '"'+obj+'"';

        return String(obj);
    }else{
        // recurse array or object
        var n,
            v,
            json = [],
            arr  = (obj && obj.constructor == Array);

        for(n in obj){
            v = obj[n];
            t = typeof(v);

            if (t == "string")
                v = '"' + v + '"';
            else if(t == "object" && v !== null)
                v = JSON.stringify(v);

            json.push((arr ? "" : '"' + n + '":') + String(v));
        }

        return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
    }
};

JSON.parse = JSON.parse || function(str){
    if(str === "")
        str = '""';
    eval("var p=" + str + ";");
    return p;
};

Array.prototype.existeValor = function(valor){
    return this.some(v => {
        return v == valor;
    });
}

class Tiempo{
    constructor(){
        this.tiempo = null;
        this.start = null;
    }

    startTimer(){
        this.start = new Date().getTime();
    }

    stopTimer(){
        return ((new Date().getTime()) - this.start) / 1000;
    }

    iniciarTiempo(){
        this.startTimer();
    }

    terminarTiempo(mensaje){
        console.log(mensaje + ': ' + this.stopTimer() + 's');
    }
}

_timer = new Tiempo();

function _(nombre, tipo, lista, enfocar){
    return new El(nombre, tipo, lista, enfocar);
}

class El{
    constructor(nombre, tipo, lista, enfoque){
        this.nombre  = nombre;
        this.tipo    = tipo || 'id';
        this.lista   = lista || false;

        this.enfoque            = enfoque || true;
        this.span               = null;
        this.mensajeValidacion  = false;

        this.asignar();
    }

    asignar(){
        var nombre  = this.nombre,
            tipo    = this.tipo,
            elm     = false;

        if(!_esObjeto(nombre)){
            switch(tipo){
                case 'tg':
                    if(_d.getElementsByTagName(nombre))
                        elm = _d.getElementsByTagName(nombre);
                    break;

                case 'nm':
                    if(_d.getElementsByName(nombre))
                        elm = _d.getElementsByName(nombre);
                    break;

                case 'cl':
                    if(_d.getElementsByClassName(nombre))
                        elm = _d.getElementsByClassName(nombre)
                    break;

                case 'id':
                    if(_d.getElementById(nombre))
                        elm = _d.getElementById(nombre);
                    break;
            }

            if(elm !== false && !this.lista && tipo != 'id')
                elm = elm[0];
        }else
            elm = nombre;

        if(!elm && _debug === true)
            console.log('Error: [' + tipo + '] ' + nombre + ' no existe.');

        this.el = elm;
    }

    existeEl(){
        return (!this.el)? false : true;
    }

    contexto(valor){
        return this.el.getContext(valor);
    }

    totalEl(){
        return (this.lista)? this.el.length : 1;
    }

    actual(num){
        return _(this.el[num]);
    }

    clonar(){
        return _(this.el.cloneNode(this.el));
    }

    reemplazar(el){
        this.el.parentNode.replaceChild(el, this.el);
        this.el = el;
        return this;
    }

    remover(){
        if(this !== null && !_esUndefined(this) && this.existeEl()){
            try{
                this.el.parentNode.removeChild(this.el);
            }catch(e){ }

            this.el = false;
        }

        return this;
    }

    padre(){
        return _(this.el.parentNode);
    }

    selElAnterior(){
        if(!this.el.previousElementSibling)
            return false;

        return _(this.el.previousElementSibling);
    }

    selElSiguiente(){
        if(!this.el.nextElementSibling)
            return false;

        return _(this.el.nextElementSibling);
    }

    totalHijos(){
        return this.el.children.length;
    }

    hijos(num){
        var num     = num || false,
            hijos   = this.el.children;

        return (num === false)? hijos : _(hijos[num - 1]);
    }

    anadirHijo(tipo, datos){
        return this.anadirAdyacente(tipo, datos, 3);
    }

    anadirAdyacente(tipo, datos, posicion){
        var tipo        = tipo || 'div',
            datos       = datos || false,
            posicion    = posicion || 4,
            el          = (!_esObjeto(tipo))? _nuevoEl(tipo, datos) : tipo;

        switch(posicion){
            case 1: posicion = 'beforebegin'; break;
            case 2: posicion = 'afterbegin'; break;
            case 3: posicion = 'beforeend'; break;
            case 4: posicion = 'afterend'; break;
        }

        this.el.insertAdjacentElement(posicion, el);
        return _(el);
    }

    // HTML del elemento
    html(html){
        if(!_esUndefined(html)){
            this.el.innerHTML = html;
            return this;
        }else
            return this.el.innerHTML;
    }

    texto(texto){
        if(!_esUndefined(texto)){
            this.el.textContent = texto;
            return this;
        }else
            return this.el.textContent;
    }

    prop(propiedad, valor){
        if(!_esUndefined(this.el[propiedad])){
            if(!_esUndefined(valor)){
                this.el[propiedad] = valor;
                return this;
            }else
                return this.el[propiedad];
        }
    }

    attr(atributo, valor){
        if(!_esUndefined(valor) || _esObjeto(atributo)){
            if(this.el.setAttribute){
                if(_esObjeto(atributo)){
                    for(var i in atributo)
                        this.el.setAttribute(i, atributo[i]);
                }else
                    this.el.setAttribute(atributo, valor);
            }

            return this;
        }else if(this.el.getAttribute)
            return this.el.getAttribute(atributo);
    }

    tieneAttr(atributo){
        if(this.el.hasAttribute)
            return this.el.hasAttribute(atributo);
        else
            return false;
    }

    quitarAttr(atributo){
        if(this.el.removeAttributeNode && this.tieneAttr(atributo))
            this.el.removeAttributeNode(atributo);

        return this;
    }

    data(nombre, valor){
        var atributo = 'data-' + nombre;

        if(!_esUndefined(valor)){
            this.attr(atributo, valor);
            return this;
        }else
            return this.attr(atributo);
    }

    dataInfo(){
        var lista   = this.el.dataset,
            data    = {},
            i;

        for(i in lista)
            data[i] = lista[i];

        return data;
    }

    // Posicion del elemento
    posicionY(){
        var el = (!this.lista)? this.el : this.el[0],
            _y = 0;

        while(el && !isNaN(el.offsetTop)){
            _y += el.offsetTop - el.scrollTop;
            el = el.offsetParent;
        }

        return _y;
    }

    posicionX(){
        var el = (!this.lista)? this.el : this.el[0],
            _x = 0;

        while(el && !isNaN(el.offsetLeft)){
            _x += el.offsetLeft - el.scrollLeft;
            el = el.offsetParent;
        }

        return _x;
    }

    scrollAlEl(pixeles){
        var posicion = this.posicionY(),
            pixeles  = pixeles || 0,
            mover    = posicion + pixeles;

        _moverScroll(mover, true);
        return this;
    }

    scrollEl(pixeles){
        if(!_esUndefined(pixeles)){
            this.el.scrollTop = pixeles;
            return this;
        }else
            this.el.scrollTop;
    }

    altura(){
        return this.el.clientHeight;
    }

    // Estilos y clases
    estilo(propiedad, valor){
        if(!_esObjeto(propiedad)){
            if(!_esUndefined(this.el.style[propiedad])){
                if(!_esUndefined(valor))
                    this.el.style[propiedad] = valor;
                else
                    return this.el.style[propiedad];
            }
        }else{
            for(var i in propiedad)
                this.el.style[i] = propiedad[i];
        }

        return this;
    }

    tieneClase(clase){
        return this.el.classList.contains(clase);
    }

    anadirClase(clase){
        var lista = (!_esObjeto(clase))? clase.split(' ') : clase,
            total = lista.length || 0;

        if(total > 0)
            lista.forEach(v => this.el.classList.add(v));

        return this;
    }

    quitarClase(clase){
        if(!_esUndefined(clase)){
            var lista = (!_esObjeto(clase))? clase.split(' ') : clase,
                total = lista.length || 0;

            if(total > 0)
                lista.forEach(v => this.el.classList.remove(v));
        }else
            this.el.className = '';

        return this;
    }

    tgClase(clase){
        if(!_esUndefined(clase)){
            var lista = (!_esObjeto(clase))? clase.split(' ') : clase,
                total = lista.length || 0;

            if(total > 0)
                lista.forEach(v => this.el.classList.toggle(v));
        }

        return this;
    }

    estaVisible(){
        return (this.estilo('display') == 'block');
    }

    estaOculto(){
        return (this.estilo('display') == 'none');
    }

    display(opcion){
        switch(opcion){
            case 0: this.estilo('display', 'none'); break;
            case 1: this.estilo('display', 'block'); break;
            case 2:
                var valor = (this.estaVisible())? 0 : 1;
                this.display(valor);
            break;
        }

        return this;
    }

    opacidad(valor){
        var aux = valor / 100;

        this.estilo({
            MozOpacity  : aux,
            opacity     : aux,
            KHTML       : aux,
            filter      : 'alpha(opacity="' + valor + '")'
        });
    }

    mostrar(tiempo, f){
        this.opacidad(0);
        this.display(1);

        var el_in       = this,
            opac_in     = 0,
            tiempo      = tiempo || 30,
            timer_in    = setInterval(function(){
                opac_in += 4;
                el_in.opacidad(opac_in);

                if(opac_in >= 100){
                    if(!_esUndefined(f)){
                        f = el_in[f] || f;
                        _ejecutar(f);
                    }

                    clearInterval(timer_in);
                }
            }, tiempo);

        return this;
    }

    ocultar(tiempo, f){
        var el_out      = this,
            opac_out    = 100,
            tiempo      = tiempo || 30,
            timer_out   = setInterval(function(){
                opac_out -= tiempo;
                el_out.opacidad(opac_out);

                if(opac_out <= 0){
                    el_out.display(0);

                    if(!_esUndefined(f)){
                        f = el_out[f] || f;
                        _ejecutar(f);
                    }

                    clearInterval(timer_out);
                }
            }, tiempo);

        return this;
    }

    anadirEvento(tipo_evento, funcion, elemento){
        var elemento = elemento || this.el;

        if(!_esArreglo(elemento))
            elemento = [elemento];

        _anadirEvento(elemento, tipo_evento, funcion);

        return this;
    }

    quitarEvento(tipo_evento, funcion, elemento){
        var elemento = elemento || this.el;

        if(!_esArreglo(elemento))
            elemento = [elemento];

        _quitarEvento(elemento, tipo_evento, funcion);

        return this;
    }

    evento(evento){
        if(_esFuncion(this.el[evento]))
            this.el[evento]();

        return this;
    }


    // Input
    nombreInp(){
        return (!this.lista)? this.el.name : this.el[0].name;
    }

    // Valor elemento
    valor(valor){
        if(!_esUndefined(valor)){
            this.el.value = valor;
            return this;
        }else
            return this.el.value.trim();
    }

    largoTexto(){
        return this.valor().length;
    }

    enMinuscula(){
        return this.valor().toLowerCase();
    }

    enMayuscula(){
        return this.valor().toUpperCase();
    }

    caracter(num){
        return this.valor().charAt(num);
    }

    soloNumero(es_rut){
        return dejarSoloNumero(this.el, es_rut);
    }

    soloFormatoNombre(con_numero, arr_aux){
        return dejarFormatoNombre(this.el, con_numero, arr_aux);
    }


    // Validacion input
    enfocar(){
        var el = (!this.lista)? this.el : this.el[0];
        el.focus();

        return this;
    }

    // Span validacion
    spanEl(){
        var nombre  = this.nombreInp() + '-sp',
            span    = _(nombre),
            attr    = {
                id      : nombre,
                class   : '-msj-val'
            };

        this.span = ((!span.existeEl())? this.anadirAdyacente('span') : span).attr(attr);
        return this;
    }

    asignarMensaje(mensaje){
        this.mensajeValidacion = mensaje || false;
        return this;
    }

    mostrarSpan(){
        var msj = this.mensajeValidacion,
            func, el;

        this.spanEl();

        if(this.span.existeEl()){
            if(!_esUndefined(msj) && msj !== false)
                this.span.texto(msj);

            this.span.display(1);
        }

        el   = this;
        func = () => el.quitarSpan();

        this.anadirEvento('blur', func)
            .anadirEvento('change', func)
            .anadirEvento('keypress', func)
            .anadirEvento('click', func);

        return this;
    }

    quitarSpan(){
        this.span.display(0);
        return this;
    }

    esFalso(texto){
        if(this.enfoque){
            if(!_esUndefined(texto))
                this.asignarMensaje(texto);

            this.enfocar()
                .mostrarSpan()
                .scrollAlEl(-140);
        }

        return false;
    }

    tieneValor(texto){
        this.asignarMensaje(texto);
        return (this.valor() != '')? true : this.esFalso();
    }

    largoTextoIgualQue(valor, texto){
        this.asignarMensaje(texto);
        return (this.largoTexto() == valor)? true : this.esFalso();
    }

    largoTextoMenorQue(valor, texto){
        this.asignarMensaje(texto);
        return (this.largoTexto() < valor)? true : this.esFalso();
    }

    largoTextoMayorQue(valor, texto){
        this.asignarMensaje(texto);
        return (this.largoTexto() > valor)? true : this.esFalso();
    }

    esNumero(texto){
        this.asignarMensaje(texto);
        return (this.valor() != '' && !isNaN(this.valor())) ? true : this.esFalso();
    }

    esMenor(valor, texto){
        this.asignarMensaje(texto);
        return (parseInt(this.valor()) < valor) ? true : this.esFalso();
    }

    esMayor(valor, texto){
        this.asignarMensaje(texto);
        return (parseInt(this.valor()) > valor) ? true : this.esFalso();
    }

    formatoNombre(texto){
        this.asignarMensaje(texto);
        return (_valNombre.test(this.valor()))? true : this.esFalso();
    }

    esCorreo(texto){
        this.asignarMensaje(texto);
        return (_valCorreo.test(this.valor()))? true : this.esFalso();
    }

    esFecha(texto){
        this.asignarMensaje(texto);
        return (_valFecha.test(this.valor()))? true : this.esFalso();
    }

    formatoRut(texto){
        this.asignarMensaje(texto);
        return (_valRut.test(this.valor()))? true : this.esFalso();
    }

    esRut(texto){
        this.asignarMensaje(texto);

        var rut  = this.valor().split('-'),
            dvr  = '0',
            suma = 0,
            mul  = 2,
            res  = 0,
            lg   = rut[0].length,
            dvi;

        for(var i = lg - 1; i >= 0; --i){
            suma += rut[0].charAt(i) * mul;
            if (mul == 7)
                mul = 2;
            else
                ++mul;
        }

        res = suma % 11;

        if(res == 1){
            dvr = 'k';
        }else if(res == 0){
            dvr = '0';
        }else{
            dvi = 11 - res;
            dvr = dvi + '';
        }

        return (_esUndefined(rut[1]) || dvr != rut[1].toLowerCase())? this.esFalso() : true;
    }

    // Checkbox o Radio
    checkeado(){
        return (this.el.checked);
    }

    checkear(valor){
        this.el.checked = valor;
    }

    valorCheckeado(texto, multiple, separador){
        if(this.lista){
            var total       = this.totalEl(),
                texto       = texto || '',
                multiple    = multiple || false,
                separador   = separador || ',',
                valor       = '',
                actual, i, aux;

            for(i = 0; i < total; ++i){
                actual = this.actual(i);

                if(actual.checkeado()){
                    aux = actual.valor()

                    if(multiple === false){
                        valor = aux;
                        break;
                    }else{
                        if(valor !== '')
                            valor += separador;

                        valor += aux;
                    }
                }
            }

            if(valor === '' && texto != '' && this.enfoque === true){
                if(!_esUndefined(texto))
                    this.asignarMensaje(texto);

                return this.esFalso();
            }

            return valor;

        }else if(_debug === true){
            console.log('No puede listar elemento ' + this.nombreInp() + ', debe habilitar lista.');
            console.log('Llamado desde: ' + this.valorCheckeado.caller.name);

            return false;
        }
    }


    // Select
    totalIndiceLista(){
        return this.el.options.length;
    }

    indiceLista(num){
        var num = num || 0;
        return this.el.options[num].text;
    }

    indiceListaSeleccionado(){
        return this.el.options[this.el.selectedIndex].text;
    }



    // Archivo
    obtenerArchivo(numero){
        var numero = numero || 0;
        return this.el.files[numero];
    }

    totalArchivos(){
        return this.el.files.length || 0;
    }

    extencionArchivo(){
        var ext = this.valor().split('.');
        return ext[ext.length - 1].toLowerCase();
    }

    pesoArchivo(numero){
        var numero = numero || 0;
        return this.propArchivo('size', numero);
    }

    esFormato(arr, texto){
        this.asignarMensaje(texto);
        return (arr.existeValor(this.extencionArchivo()))? true : this.esFalso();
    }

    propArchivo(propiedad, numero){
        var numero  = numero || 0,
            archivo = this.obtenerArchivo(numero);

        return archivo[propiedad];
    }



    // Form
    cantidadElementos(){
        return this.el.elements.length || 0;
    }

    elementos(num){
        var els = this.el.elements,
            arr = [],
            total, i;

        if(_esUndefined(num)){
            total = this.cantidadElementos();
            if(total > 0){
                for(i = 0; i < total; ++i)
                    arr.push(_(els[i]));
            }

            return arr;
        }else
            return (els[num])? _(els[num]) : false;
    }

    enviar(){
        this.el.submit();
        return this;
    }

    resetear(){
        this.el.reset();
        this.elementos(0).enfocar();
        return this;
    }
}


function _objForm(datos){
    var fm      = new FormData(),
        datos   = datos || {},
        i;

    for(i in datos)
        fm.append(i, datos[i]);

    return fm;
}

function _ax(parametro){
    return new Ajax(parametro);
}

class Ajax{
    constructor(parametro){
        this.accion                 = parametro.accion || 3;
        this.url                    = parametro.url;
        this.datos                  = parametro.datos || {};
        this.funcion                = parametro.funcion || null;
        this.nombre                 = parametro.nombre || '';
        this.async                  = (_esUndefined(parametro.async))? true : false;
        this.respuesta              = '';
        this.div_respuesta          = parametro.div_respuesta || false;
        this.iniciar                = parametro.iniciar || true;
        this.barra                  = parametro.barra || null;
        this.porcentaje             = parametro.porcentaje || null;
        this.siempre                = parametro.siempre || null;
        this.div_resp               = null;
        this.termino_envio_datos    = parametro.termino_envio_datos || null;
        this.tiempo_inicio          = null;

        this.datos.url_visita       = _urlActual();

        this.xhr = function(){
            var xmlhttp = false;
            try{
                xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
            }catch(e){
                try {
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }catch(ex){ }
            }

            if (!xmlhttp && !_esUndefined(XMLHttpRequest))
                xmlhttp = new XMLHttpRequest();

            return xmlhttp;
        }

        if(this.iniciar === true)
            this.ejecutar();
    }

    ejecutar(){
        if(_debug === true)
            this.tiempo_inicio = performance.now();

        var ajax                = this,
            xhr                 = ajax.xhr(),
            barra               = this.barra,
            porcentaje          = this.porcentaje,
            termino_envio_datos = this.termino_envio_datos,
            datos               = {},
            avance, aux, i;

        try{
            xhr.addEventListener('loadstart', function(){
                if(barra != null){
                    barra = _(barra);
                    barra.estilo('width', '0%');
                }

                if(porcentaje != null){
                    porcentaje = _(porcentaje);
                    porcentaje.texto('0%');
                }
            }, false);

            xhr.upload.addEventListener('progress', function(e){
                if((barra !== null || porcentaje !== null) && e.lengthComputable){
                    avance = parseInt(e.loaded / e.total * 100);

                    if(barra !== null)
                        barra.estilo('width', avance + '%');

                    if(porcentaje !== null)
                        porcentaje.texto(avance + '%');

                    if(termino_envio_datos !== null && avance == 100)
                        _divCargaAjax(1, termino_envio_datos);
                }
            }, false);

            xhr.addEventListener('load', function(e){
                if(barra !== null)
                    barra.estilo('width', '100%');

                if(porcentaje !== null)
                    porcentaje.texto('100%');

                if(_esFuncion(ajax.siempre))
                    ajax.siempre();

                if(xhr.status == 200){
                    ajax.respuesta  = xhr.responseText;
                    aux             = ajax.separaRespuesta();

                    ajax.devuelveRespuesta();

                    if(aux[0] == 99){
                        setTimeout(_redir, segundos(3));
                        return false;
                    }
                }else{
                    if(_debug === true)
                        _msj(0, '[Ajax] ' + xhr.status + ' &nbsp;' + xhr.statusText + '<br>' + xhr.responseURL);
                    else
                        _msj(0, '[JS] Ha ocurrido un error<br>Favor contacte al encargado');
                }
            }, false);

            for(i in ajax.datos){
                datos[i] = (typeof ajax.datos[i] === 'string')?
                    ajax.datos[i].extraerScript()
                :   ajax.datos[i];
            }

            xhr.open('POST', ajax.url, ajax.async);
            xhr.send(_objForm(datos));
        }catch(err){
            if(_debug === true)
                console.log('[Ajax] Error de carga: ' + err);
        }
    }

    devuelveRespuesta(){
        if(_debug === true){
            console.log('');
            console.info('[Ajax]\t' + this.nombre,
                '\nTiempo: ' + ((performance.now() - this.tiempo_inicio) / 1000).toFixed(2) + ' seg.',
                '\nRespuesta: ' + this.respuesta
            );
        }

        var funcion = this.funcion,
            respuesta;

        if(this.div_respuesta){
            respuesta = this.separaRespuesta();

            if(!respuesta)
                return false;

            this.mostrarRespuesta(respuesta[0], respuesta[1]);
        }

        if(funcion !== null){
            switch(this.accion){
                // soloEjecuta // ejecutar(funcion);
                case 1: respuesta = null; break;

                // recibeRespuesta
                case 2: respuesta = this.separaRespuesta(); break;

                // recibeJSON
                case 3: respuesta = this.respuestaJSON(); break;

                // recibeContenido
                case 4: respuesta = this.respuesta; break;
            }

            if(_debug === true)
                console.log('Formato: ', respuesta);

            if(respuesta === false)
                return false;

            _ejecutar(funcion, respuesta);
        }
    }

    separaRespuesta(){
        try{
            return this.respuesta.split('||');
        }catch(err){
            if(_debug === true)
                console.log('[Ajax] Error al separar respuesta: ' + err);

            this.mostrarRespuesta(0);

            if(this.funcion !== null)
                _ejecutar(this.funcion, this.respuesta);

            return false;
        }
    }

    respuestaJSON(){
        try{
            return JSON.parse(this.respuesta);
        }catch(err){
            if(_debug === true)
                console.log('[Ajax] Error al parsear JSON: ' + err);

            this.mostrarRespuesta(0);

            if(this.funcion !== null)
                _ejecutar(this.funcion, this.respuesta);

            return false;
        }
    }

    mostrarRespuesta(numero, texto){
        var aux     = numero || '',
            texto   = texto || aux + this.respuesta,
            numero  = numero || 0;

        if(numero != '' && isNaN(numero)){
            texto   = numero;
            numero  = 0;
        }

        texto = texto.split('||');

        if(texto.length > 1){
            numero  = texto[0];
            texto   = texto[1];
        }else
            texto   = texto[0];

        _msj(numero, texto);
    }
}

function _msj(numero, texto){
    return new DivRespuesta(numero, texto);
}

class DivRespuesta{
    constructor(numero, texto){
        var ntf     = _('-notificacion-ajax'),
            numero  = numero || 0,
            texto   = texto || '',
            listado = _('div-msj', 'cl', true),
            total   = (listado.existeEl())? listado.totalEl() : 0,
            resp, div, _t1, _t2;

        if(texto.trim() == '')
            return false;

        if(total > 2)
            listado.actual(0).remover();

        numero  = (parseInt(numero) === 1)? 1 : 0;
        div     = ntf.anadirHijo('div').anadirClase('div-msj div-msj-' + numero + ' radius-3 shadow-1 transition-03');

        this.btn = div.anadirHijo('i', {title: 'Cerrar'}).anadirClase('icon-cross2 cerrar');
        this.p   = div.anadirHijo('p').html(texto);
        this.div = div;

        resp = this;
        this.btn.anadirEvento('click', () => { resp.cerrarRespuesta(); });

        _t1 = setTimeout(function(){
            if(resp.div.existeEl())
                resp.div.anadirClase('e-mov');
        }, 100);

        _t2 = setTimeout(function(){
            if(resp.div.existeEl())
                resp.cerrarRespuesta();
        }, segundos(20));
    }

    cerrarRespuesta(){
        if(this.btn.existeEl())
            this.div.ocultar(5, 'remover');
    }
}


function _gr(d){
    return new Grafico(d);
}

function _iniciarGrafico(graficos){
    var g = graficos || [],
        t = g.length || 0,
        i;

    if(t > 0){
        for(i = 0; i < t; ++i)
            _gr(g[i]);
    }
}

function _actualizarDatosGrafico(graficos){
    var g = graficos || [],
        t = g.length || 0,
        i, a;

    if(t > 0){
        for(i = 0; i < t; ++i){
            a = g[i];

            if(!_esUndefined(_w[a.id]))
                _w[a.id].actualizarDatos(a.valores);
        }
    }
}

class Grafico extends El{
    constructor(p){
        var id = p.id;

        super(id);
        super.asignar();

        this.id         = id;
        this.tipo       = p.tipo || 'bar';
        this.titulos    = p.titulos || [];
        this.valores    = p.valores || [];

        this.responsive = p.responsive || true;
        this.padding    = p.padding || 10;
        this.tooltips   = {};
        this.scales     = {};
        this.plugins    = {};
        this.legend     = p.leyenda || false;

        this.legend_position    = p.posicion_leyenda || 'right';
        this.mostrar_datalabel  = p.mostrar_datalabel || false;

        switch(this.tipo){
            case 'line':
            case 'bar':
                this.scales = {
                    yAxes: [{
                        ticks: {
                            // Include a dollar sign in the ticks
                            callback    : function(value, index, values){ return puntos(value); },
                            beginAtZero : p.cero_y || true,
                            fontColor   : p.color_y || '#000'
                        }
                    }],

                    xAxes: [{
                        ticks: {
                            fontColor : p.color_x || '#000'
                        }
                    }]
                };
            break;
        }

        this.plugins = {
            // https://emn178.github.io/chartjs-plugin-labels/samples/demo/
            labels: {
                render      : p.render || 'percentage',
                fontColor   : p.color_porcentaje || '#000',
                precision   : p.precision_porcentaje || 2
            },
            datalabels: {
                display: this.mostrar_datalabel
            }
        };

        console.log('Plugins: ', this.id, this.plugins);
        this.iniciar();
    }

    iniciar(){
        if(this.tipo != 'line' && this.tipo != 'bar'){
            this.tooltips = {
                callbacks: {
                    label: function(tooltipItem, data){
                        var allData         = data.datasets[tooltipItem.datasetIndex].data,
                            tooltipLabel    = data.labels[tooltipItem.index],
                            tooltipData     = allData[tooltipItem.index];

                        return tooltipLabel + ': ' + puntos(tooltipData);
                    }
                }
            };
        }else{
            this.tooltips = {
                callbacks: {
                    label: function(tooltipItem, data){
                        var valor = data.datasets[tooltipItem.datasetIndex].label || '';

                        if(valor)
                            valor += ': ';

                        valor += puntos(tooltipItem.yLabel);

                        return valor
                    }
                }
            };
        }

        this.grafico = new Chart(this.contexto('2d'), {
            type: this.tipo,
            data: {
                labels      : this.titulos,
                datasets    : this.formatearValores()
            },
            options: {
                layout: { // Cuerpo del gráfico
                    padding: this.padding
                },
                responsive  : this.responsive,
                legend      : {
                    display     : this.legend,
                    position    : this.legend_position
                },
                tooltips    : this.tooltips,
                scales      : this.scales,
                plugins     : this.plugins
            }
        });

        _w[this.id] = this;
    }

    formatearValores(v){
        var v = v || this.valores,
            t = v.length || 0,
            arr = [],
            i, a;

        for(i = 0; i < t; i++){
            a = v[i];

            if(!_esUndefined(a.valores)){
                arr[i] = {
                    label           : a.nombre || null,
                    backgroundColor : a.fondo || '',
                    borderColor     : a.color || '',
                    data            : a.valores
                };
            }else
                arr[i] = a;
        }

        return arr;
    }

    actualizarDatos(v){
        var t = v.titulos || false,
            v = v.valores || v,
            i = 0;

        if(v.length == 0)
            v = this.valores;

        if(t !== false)
            this.grafico.data.labels = t;

        v = this.formatearValores(v);

        this.grafico.data.datasets.forEach((d) => {
            d.data = v[i];
            ++i;
        });

        this.grafico.update();
    }
}

function _inputFile(inp){
    var inp     = _(inp),
        txt     = inp.selElSiguiente(),
        valor   = (inp.valor() != '')? inp.valor() : 'Ningún archivo seleccionado';

    if(txt !== false)
        txt.html(valor);
}

function _scrollHtml(opcion){
    var html    = _('html', 'tg'),
        body    = _('body', 'tg'),
        valor   = (opcion == 1)? 'auto' : 'hidden';

    html.estilo('overflow', valor);
    body.estilo('overflow', valor);
}

var _arrFix      = [],
    _totalArrFix = 0;

function _fondoFix(parametro){
    var parametro           = parametro || {},
        contenido           = parametro.contenido || '',
        accion              = (contenido !== '')? 1 : 0,
        clases              = parametro.clases || false,
        compartir_clases    = parametro.compartir_clases || false,
        mostrar_overflow    = parametro.overflow || false,
        bloquear            = parametro.bloquear || false,
        retroceder          = parametro.retroceder || false,
        nombre              = parametro.nombre || '',
        ejecutar            = parametro.ejecutar || false,

        div_fondo           = _('fondo-opaco'),
        div_fix             = _('fondo-fix'),
        div_principal       = _('fondo-principal'),
        div_contenido       = _('contenido-fix'),
        clases_aux          = 'w-90 bg-f',
        tiene_contenido, aux;

    div_fix.quitarClase();
    if(accion === 0){
        _arrFix      = [];
        _totalArrFix = 0;

        _totalGaleria   = 0;
        _imgGaleria     = 0;

        if(div_fondo.estaVisible())
            div_fondo.ocultar(2);

        div_contenido.quitarClase().html('');
    }else if(accion === 1){
        if(retroceder === true){
            var cargar_anterior = true,
                nombre_anterior = '';

            aux             = div_contenido.html();
            tiene_contenido = (aux !== '' && aux != contenido);

            if(_totalArrFix > 0){
                nombre_anterior = _arrFix[_totalArrFix - 1].nombre;
                if(nombre_anterior === nombre)
                    cargar_anterior = false;
            }

            if(cargar_anterior === true && tiene_contenido === true){
                _totalArrFix = _arrFix.push({
                    nombre  : nombre,
                    html    : div_contenido.clonar()
                });
            }

            _mostrarContenidoFixAnterior();
        }

        if(!clases)
            clases = clases_aux;
        else if(compartir_clases !== false)
            clases += ' ' + clases_aux;

        if(bloquear === false)
            div_principal.anadirEvento('click', _fondoFix);
        else
            clases += ' fix-bloqueado';

        div_contenido.quitarClase()
            .anadirClase(clases + ' scroll-custom radius-3 shadow-1')
            .html(contenido)
            .scrollEl(0);

        if(mostrar_overflow === false)
            div_contenido.estilo('overflow', 'auto');

        div_fix.anadirClase('visible');

        if(!div_fondo.estaVisible())
            div_fondo.mostrar(2);
    }

    if(ejecutar !== false){
        if(!_esArreglo(ejecutar))
            ejecutar = [[ejecutar]];

        _ejecutarListado(ejecutar);
    }
}

function _mostrarContenidoFixAnterior(){
    var btn = _('atras-fondo-fix'),
        ds  = (_arrFix.length > 0)? 1 : 0,
        div = _('contenido-fix'),
        _t;

    _t = setTimeout(function(){
        btn.display(ds)
            .estilo({
                top  : div.posicionY() + 'px',
                left : div.posicionX() + 'px'
            });
        clearInterval(_t);
    }, 10);
}

function _anteriorContenidoFix(){
    var div      = _('contenido-fix'),
        anterior = _arrFix[_totalArrFix - 1].html;

    div.reemplazar(anterior.el);
    _arrFix.pop();
    _totalArrFix = _arrFix.length;

    _mostrarContenidoFixAnterior();
}


var _imagenesGaleria = [],
    _totalImgGaleria = 0,
    _imgActual       = 0;

function _galeria(imagenes, num){
    var imagenes    = imagenes || [],
        num         = num || 0,
        html        = '<div id="-img-galeria-fix"></div>';

    _imagenesGaleria = imagenes;
    _totalImgGaleria = _imagenesGaleria.length;
    _imgActual       = num;

    if(_totalImgGaleria < 1)
        return false;

    if(_totalImgGaleria > 1){
        html += '<button' + _htmlAttr({
            id      : '-btn-anterior',
            class   : '-btn-galeria icon-chevron-thin-left shadow-1',
            onclick : '_imgAnterior()'
        }) + '></button>'
        + '<button' + _htmlAttr({
            id      : '-btn-siguiente',
            class   : '-btn-galeria icon-chevron-thin-right shadow-1',
            onclick : '_imgSiguiente()'
        }) + '></button>';
    }

    _fondoFix({
        contenido   : html,
        clases      : '-galeria-fix'
    });

    _imagenGaleria();
}

function _htmlObjImagen(imagen){
    var imagen = imagen || false,
        aux     = '',
        href    = '',
        html    = '',
        actual;

    if(!imagen)
        return html;

    if(_esObjeto(imagen)){
        for(i in imagen){
            actual = ' ' + i + '="' + imagen[i] + '"';

            if(i == 'href' || i == 'target')
                href += actual;
            else
                aux += actual;
        }

        if(href != '')
            html += '<a' + href + '>';

        html += '<img' + aux + '>';

        if(href != '')
            html += '</a>';
    }else
        html = '<img src="' + imagen + '">';

    return html;
}

function _imagenGaleria(){
    var imagen  = _imagenesGaleria[_imgActual],
        div     = _('-img-galeria-fix'),
        html    = _htmlObjImagen(imagen);

    div.html(html);
}

function _imgAnterior(){
    if(_imgActual - 1 < 0)
        _imgActual = (_totalImgGaleria - 1);
    else
        --_imgActual;

    _imagenGaleria();
}

function _imgSiguiente(){
    if(_imgActual + 1 >= _totalImgGaleria)
        _imgActual = 0;
    else
        ++_imgActual;

    _imagenGaleria();
}

function _divCargaAjax(opcion, html){
    var div     = _('-img-carga'),
        texto   = _('-texto-img-carga'),
        opcion  = opcion || 0,
        html    = html || '';

    if(opcion == 1){
        if(!div.estaVisible())
            div.mostrar(8);
    }else
        div.ocultar(8);

    texto.html(html);
}

function _textoDeCarga(texto, carga){
    var carga = carga || '';
    return carga + '<br><div class="txt-center">' + texto + '</div>';
}

function _cargarSelect(actual){
    var actual  = _(actual),
        valor   = actual.valor(),
        info    = actual.dataInfo(),
        id      = info.id,
        sl      = _(id);

    sl.html('<option value="">' + sl.indiceLista() + '</option>')
        .prop('disabled', true);

    if(valor === '')
        return false;

    _ax({
        nombre  : 'Cargar select',
        accion  : 3,
        url     : _dir + 'php/' + info.dir + '.php',
        funcion : function(datos){
            var input       = _(datos.input),
                lista       = datos.lista || [],
                total       = lista.length || 0,
                html        = '<option value="">' + input.indiceLista() + '</option>',
                bloquear    = true;

            if(total > 0){
                bloquear = false;
                lista.forEach(v => html += '<option value="' + v.id + '">' + v.nombre + '</option>');
            }

            input.html(html)
                .prop('disabled', bloquear)
                .evento('onchange');
        },
        datos   : {
            accion  : info.accion,
            input   : id,
            valor   : valor
        }
    });
}

function _multiSeleccion(el){
    var el      = _(el),
        check   = (el.checkeado() == true),
        nombre  = el.data('nombre'),
        inputs  = _(nombre, 'nm', true),
        total   = inputs.totalEl(),
        i;

    if(total > 0){
        for(i = 0; i < total; ++i)
            inputs.actual(i).checkear(check);
    }
}

function _mostrarOcultar(nombre){
    var f   = _(nombre),
        ds  = (f.estaVisible());

    if(ds)
        f.display(0);
    else
        f.mostrar(10);
}

function _agregarVisita(){
    _ax({
        nombre  : 'Visita',
        accion  : 1,
        url     : _dir + 'php/ax-visita.php',
        funcion : function(datos){
            var _t = setTimeout(function(){
                _agregarVisita();
                clearTimeout(_t);
            }, segundos(5));
        },
        datos   : {
            accion      : 'agregar-visita',
            url_visita  : _urlActual(),
            referencia  : _urlReferencia(),
            visible     : (_wFocus)? 1 : 0
        }
    });
}

function _agregarAccion(id_accion){
    _ax({
        nombre  : 'Accion',
        accion  : 1,
        url     : _dir + 'php/ax-visita.php',
        datos   : {
            accion      : 'accion-visita',
            id_accion   : id_accion,
            url_visita  : _urlActual()
        }
    });
}

function _ventanaJS(url, nombre, parametros, imprimir){
    var _ventana = _w.open(url, nombre, parametros),
        imprimir = imprimir || false;

    _ventana.focus();

    if(imprimir === true)
        _ventana.print();
}

function _generarForm(datos){
    _divCargaAjax();

    var datos   = datos || {},
        enviar  = datos.enviar || true,
        quitar  = datos.quitar || true,
        inputs  = datos.inputs || [],
        total   = inputs.length,
        form    = datos.form || false,
        funcion = datos.funcion || false,
        i, _t;

    if(!form)
        return false;

    form = _('body', 'tg').anadirHijo('form', datos.form);

    if(total > 0)
        inputs.forEach(i => form.anadirHijo('input', i));

    if(enviar === true)
        form.enviar();

    if(quitar === true)
        form.remover();

    if(funcion !== false ){
        _t = setTimeout(function(){
            if(!_esArreglo(funcion))
                funcion = [[funcion]];

            _ejecutarListado(funcion);
            clearTimeout(_t);
        }, segundos(1));
    }
}

function _imagenDinamica(){
    var ancho   = _anchoDispositivo(),
        img     = _('-img-resp', 'cl', true),
        total   = img.totalEl(),
        arr_d   = [992, 769, 600],
        j, i, actual, datos, src, dim, aux, src_aux, cambiar;

    if(total > 0){
        for(i = 0; i < total; ++i){
            actual  = img.actual(i);
            datos   = actual.dataInfo();
            src     = actual.attr('src');
            cambiar = false;
            src_aux = datos['src'];

            for(j in arr_d){
                dim = arr_d[j];

                if(ancho > dim)
                    break;

                if(dim == 600)
                    aux = 'xs';
                else if(dim == 769)
                    aux = 'sm';
                else if(dim == 992)
                    aux = 'md';

                if(ancho < dim && !_esUndefined(datos[aux]))
                    src_aux = datos[aux];
            }

            if(src != src_aux)
                actual.attr('src', src_aux);
        }
    }
}

function _fechaEnMiliSeg(fecha){
    var fecha = fecha || false;

    fecha = (fecha !== false)? new Date(fecha) : new Date();
    return fecha.getTime();
}

function _anteponerCero(num){
    if(num < 10){
        if(num < 0)
            num = 0;

        num = '0' + num;
    }

    return num;
}

function _iniciarContador(){
    var div     = _('-contador', 'cl', true),
        total   = div.totalEl(),
        i;

    if(total > 0){
        for(i = 0; i < total; ++i)
            div.actual(i).padre().anadirHijo('div');

        _actualizarContador(div);
    }
}

function _actualizarContador(lista){
    var lista = lista || false,
        total, i;

    if(!lista)
        return false;

    total = lista.totalEl();
    if(total > 0){
        const fecha_actual  = _fechaEnMiliSeg(),
            segundo         = 1000,
            minuto          = segundo * 60,
            hora            = minuto * 60,
            dia             = hora * 24;

        var actual, data, fecha_fin, fecha_inicio, fecha_prev,
            diferencia, dias, datos, html, texto, j;

        for(i = 0; i < total; ++i){
            actual      = lista.actual(i);
            data        = actual.dataInfo();
            fecha_fin   = data.fechaTermino || false;

            if(fecha_fin !== false){
                fecha_fin       = _fechaEnMiliSeg(fecha_fin);
                fecha_prev      = data.fechaPrevia || false;
                fecha_inicio    = data.fechaInicio || false;
                texto           = data.textoInicio || '';
                datos           = {};
                html            = '';

                if(fecha_inicio !== false){
                    fecha_inicio = _fechaEnMiliSeg(fecha_inicio);

                    if(fecha_prev !== false && (
                        (_fechaEnMiliSeg(fecha_prev)) < fecha_actual &&
                        fecha_actual < fecha_inicio
                    )){
                        fecha_fin   = fecha_inicio;
                        texto       = data.textoPrevia || '';
                    }
                }

                if(fecha_fin < fecha_actual)
                    texto = data.textoTermino || '';

                diferencia  = fecha_fin - fecha_actual;
                dias        = Math.floor(diferencia / dia);

                if(dias > 0)
                    datos.DIAS = _anteponerCero(dias);

                datos = _concatenarObj([datos,{
                    HORAS       : _anteponerCero(Math.floor((diferencia % dia) / hora)),
                    MINUTOS     : _anteponerCero(Math.floor((diferencia % hora) / minuto)),
                    SEGUNDOS    : _anteponerCero(Math.floor((diferencia % minuto) / segundo))
                }]);

                for(j in datos)
                    html += '<div class="ds-cell"><p>' + datos[j] + '</p>' + j + '</div>';

                html = '<div class="ds-table">' + html + '</div>';

                if(texto !== '')
                    html = '<h5>' + texto + '</h5>' + html;

                actual.selElSiguiente().html(html);
            }
        }

        setTimeout(function(){
            _actualizarContador(lista);
        }, segundo);
    }
}

function _inputFechaHora(p){
    //https://xdsoft.net/jqplugins/datetimepicker/
    var input            = p.input,
        input_2          = p.input_2 || false,
        fecha_minima     = p.fecha_minima || '2000/01/01',
        fecha_maxima     = p.fecha_maxima || '2500/01/01',
        min_ayer         = p.min_ayer || false,
        max_ayer         = p.max_ayer || false,
        formato          = p.formato || 'd/m/Y',
        con_hora         = p.con_hora || false,
        intervalo_minuto = p.intervalo_minuto || 5,
        anio_inicio      = p.anio_inicio || '1950',
        anio_termino     = p.anio_inicio || '2050',
        deshabilitar_dia = p.deshabilitar_dia || [], // 0-6
        meses            = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        dias             = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
        dias_corto       = ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
        ayer             = '-1970/01/2',
        maniana          = '+1970/01/2',

        min_dia_siguiente = p.min_dia_siguiente || false,
        max_dia_siguiente = p.max_dia_siguiente || false,
        fecha;

    if(min_ayer === true)
        fecha_minima = ayer;

    if(max_ayer === true)
        fecha_maxima = ayer;

    if(min_dia_siguiente === true)
        fecha_minima = maniana;

    if(max_dia_siguiente === true)
        fecha_maxima = maniana;

    if(input_2 !== null){
        input_2 = $(input_2).attr('autocomplete', 'off')
            .on('focus', function(){ $(this).val(''); })
            .on('keydown', function(){ return false; });
    }

    $(input).datetimepicker({
        i18n: {
            en: {
                months:         meses,
                dayOfWeek:      dias,
                dayOfWeekShort: dias_corto
            }
        },

        format          : formato,
        timepicker      : con_hora,
        dayOfWeekStart  : 1,
        minDate         : fecha_minima,
        maxDate         : fecha_maxima,
        yearStart       : anio_inicio,
        yearEnd         : anio_termino,
        step            : intervalo_minuto,
        disabledWeekDays: deshabilitar_dia,

        onSelectDate:   function(tiempo, $i){
            if(input_2 !== false){
                fecha = $i.val().split(' ');
                fecha = fecha[0].split('/');
                fecha = fecha[2] + '-' + fecha[1] + '-' + fecha[0];

                input_2.datetimepicker({
                    i18n: {
                        en: {
                            months:         meses,
                            dayOfWeek:      dias,
                            dayOfWeekShort: dias_corto
                        }
                    },

                    format          : formato,
                    timepicker      : con_hora,
                    dayOfWeekStart  : 1,
                    minDate         : fecha,
                    maxDate         : fecha_maxima,
                    yearStart       : anio_inicio,
                    yearEnd         : anio_termino,
                    step            : intervalo_minuto,
                    disabledWeekDays: deshabilitar_dia
                });
            }
        }
    })
    .attr('autocomplete', 'off')
    .on('keydown', function(){ return false; })
    .on('focus', function(){
        $(this).val('');

        if(input_2 !== null)
            input_2.val('');
    });
}

function _htmlSelect(listado, id){
    var listado = listado || [],
        total   = listado.length || 0,
        id      = id || false,
        html    = '',
        sel;

    if(total > 0){
        listado.forEach(v => {
            sel     = (id !== false && id == v.id)? 'selected="selected"' : '';
            html    += '<option value="' + v.id + '" ' + sel + '>' + v.nombre + '</option>';
        });
    }

    return html;
}

function _htmlCampo(input, nombre){
    /*var nombre      = nombre || '',
        atributo    = atributo || '',
        html;

    html = '<div class="ps-rel">';

        html += (nombre != '')? '<div class="nombre-campo">' + nombre + '</div>' : '<label>';
        html += input;

        if(nombre != '')
            html += '</label>';

    html += '</div>';

    return html;*/

    var input   = input || {},
        nombre  = nombre || '',
        html    = '';

    if(!_esObjeto(input)){
        html = '<div class="ps-rel">';

        html += (nombre != '')? '<div class="nombre-campo">' + nombre + '</div>' : '<label>';
        html += input;

        if(nombre != '')
            html += '</label>';

        html += '</div>';
    }else{
        var tipo        = input.tipo || false;
            val_tipo    = (tipo !== false && tipo !== 'check')? tipo : false;
            apertura    = (!val_tipo)? 'input' : tipo;
            cierre      = (!val_tipo)? '' : '</' + tipo + '>';
            campo       = '';
            aux         = '';
            texto       = input.texto || '',
            contenedor  = '';

        if(tipo == 'select' && !_esUndefined(input.listado)){
            id  = input['listado-id'] || 0;
            aux = _htmlSelect(input.listado, id);
        }

        nombre  = input['nombre-campo'] || '';
        campo   = '<' + apertura
                + _htmlAttr(input, ['tipo', 'listado', 'listado-id', 'nombre-campo', 'texto'])
            + '>' + aux + texto + cierre;

        if(tipo == 'check'){
            campo       += '<span class="inp-chk">' + nombre + '</span>';
            nombre      = '';
            contenedor  = 'label>';
        }

        html = (contenedor != '')? contenedor : 'div class="ps-rel">';

        if(nombre != '')
            nombre = '<div class="nombre-campo">' + nombre + '</div>';

        html = '<' + html + nombre + campo + '</';
        html += (contenedor != '')? contenedor : 'div>';
    }

    return html;
}

function _htmlForm(datos){
    var form        = datos.form || false,
        inputs      = datos.inputs || datos,
        total_input = inputs.length || 0,
        botones     = datos.botones || false,
        html        = '';

    if(total_input > 0)
        inputs.forEach(i => html += _htmlCampo(i));

    if(botones !== false)
        html += '<br><br>' + _htmlListadoBoton(botones);

    if(form !== false){
        if(html !== '')
            html += '</form>';

        html = '<form' + _htmlAttr(form) +'>' + html;
    }

    return html;
}


function _htmlCabeceraListado(fila){
    var fila    = fila || {},
        html    = '',
        total   = 0,
        i, actual, valor, prop, j;

    fila = _arregloAObjeto(fila);

    for(i in fila){
        ++total;

        actual  = fila[i];
        valor   = '';
        prop    = '';

        if(_esObjeto(actual)){
            valor = actual.valor || '';
            prop  = '';

            for(j in actual){
                if(j != 'valor')
                    prop += ' ' + j + '="' + actual[j] + '"';
            }
        }else if(_esString(actual))
            valor = actual;

        if(valor != '')
            html += '<th' + prop + '>' + valor + '</th>';
    }

    return {html, total};
}

function _htmlFilaListado(fila, estilos, otros){
    var fila    = fila || {},
        estilos = estilos || {},
        otros   = otros || {},
        html    = '',
        i, actual, prop, clase, lleva_clase, otro, valor, j, aux;

    fila = _arregloAObjeto(fila);

    for(i in fila){
        actual      = fila[i];
        clase       = estilos[i] || '';
        lleva_clase = (clase != '' && clase != null);
        otro        = otros[i] || '';
        prop        = '';

        if(_esObjeto(actual)){
            valor = actual.valor || '';

            for(j in actual){
                aux = actual[j];

                if(j != 'valor'){
                    if(j == 'class'){
                        if(lleva_clase === true)
                            clase += ' ' + aux;
                        else
                            clase = aux;
                    }else
                        prop += ' ' + j + '="' + aux + '"';
                }
            }
        }else{
            valor   = actual;
            prop    = otro;
        }

        prop += (clase != '')? ' class="' + clase + '"' : '';
        html += '<td ' + prop + '>' + valor + '</td>';
    }

    return html;
}

function _htmlListado(datos, ocultar_resultados){
    var clases              = datos.clases || '',
        cabecera            = datos.cabecera || [],
        sin_datos           = datos.sin_datos || 'No existen registros',
        estilos             = datos.estilos || {},
        otros               = datos.otros || {},
        listado             = datos.listado || [],
        total               = listado.length,
        ocultar_resultados  = ocultar_resultados || false,
        resultados          = (!ocultar_resultados)? _htmlResultado(total) : '',
        html                = '';

    cabecera    = _htmlCabeceraListado(cabecera);
    html        = resultados + '<div class="-tb-listado ' + clases + '">'
        + '<table>'
            + '<tr>' + cabecera.html + '</tr>';

            if(total > 0)
                listado.forEach(fila => html += '<tr>' + _htmlFilaListado(fila, estilos, otros) + '</tr>');
            else
                html += '<tr><td colspan="' + cabecera.total + '" class="txt-center">' + sin_datos + '</td></tr>';

        html += '</table>'
    + '</div>';

    return html;
}

function _htmlResultado(total){
    var plural = (total != 1)? 's' : '';

    return '<div class="-total-resultado"><div>' + puntos(total) + ' resultado' + plural + '</div></div>';
}

function _htmlListadoBoton(botones){
    var botones = botones || [],
        total   = botones.length || 0,
        html    = '',
        nombre;

    if(total > 0){
        html = '<div class="-listado-boton">';

            botones.forEach(b => {
                nombre  = b.nombre || '';
                html    += '<button' + _htmlAttr(b, ['nombre']) + '>' + nombre + '</button>';
            });

        html += '</div>';
    }

    return html;
}

function _htmlListadoInformacion(datos){
    var titulo  = datos.titulo || '',
        listado = datos.listado || datos,
        html    = '',
        i, actual;

    if(listado !== false){
        html = titulo;

        for(i in listado){
            actual = listado[i];

            if(_esString(actual)){
                html += '<div class="nombre-campo">' + i + '</div>'
                    + '<p>' + actual + '</p>';
            }
        }
    }

    return html;
}

function _popupIFrame(elemento, clases){
    if(_esUndefined(elemento))
        return false;

    var elemento    = _(elemento),
        clases      = clases || '',
        html        = _htmlAttr(elemento.dataInfo());

    _fondoFix({
        contenido   : '<iframe' + html + '></iframe>',
        clases      : clases,
        retroceder  : true
    });
}

function _autocargaJS(body, archivos){
    var a = archivos[0],
        c = false,
        s = _nuevoEl('script', {
            type: 'text/javascript',
            src : a
        }), t;

    s.onload = s.onreadystatechange = () => {
        if(!c && (!this.readyState || this.readyState == 'complete')){
            if(_debug === true)
                console.log(archivos);

            archivos.splice(0, 1);

            c = true;
            t = archivos.length || 0;

            if(t > 0)
                _autocargaJS(body, archivos);
            else
                _init();
        }
    }

    if(_debug === true)
        console.log(a);

    body.anadirHijo(s);
}

function _init(){
    _imagenDinamica();
    //_agregarVisita();

    if(_existeFuncion('cargaInicialGeneral'))
        cargaInicialGeneral();
}

_anadirEvento(_w, 'focus', () => _wFocus = true );
_anadirEvento(_w, 'blur', () => _wFocus = false );

_anadirEvento(_w, 'load', () => {
    var total = _archivosJS.length || 0;

    if(total > 0)
        _autocargaJS(_('body', 'tg'), _archivosJS);
    else
        _init();
});

_anadirEvento(_w, 'resize', () => {
    _imagenDinamica();
    _mostrarContenidoFixAnterior();

    if(_existeFuncion('alRedimensionar'))
        alRedimensionar();
});

_anadirEvento(_d, 'keydown', () => {
    var e       = event,
        tecla   = e.key;

    if(_debug === true)
        console.log('Tecla: ', tecla, ', Evento: ', e);

    switch(tecla){
        case 'Escape':
            if(!_('contenido-fix').tieneClase('fix-bloqueado'))
                _fondoFix();
        break;

        case 'ArrowLeft':
            if(_('btn-anterior').existeEl())
                _imgAnterior();
        break;

        case 'ArrowRight':
            if(_('btn-siguiente').existeEl())
                _imgSiguiente();
        break;
    }
});
